#!/bin/bash

DEFAULTCONSULVERSION="1.8.2"
DEFAULTDATACENTER="us-west-1"
DEFAULTRESOLVER="10.0.0.2"

read -p "Enter Consul version to use [$DEFAULTCONSULVERSION]: " CONSULVERSION
CONSULVERSION=${CONSULVERSION:-$DEFAULTCONSULVERSION}

read -s -p "Enter Consul encryption key (run consul keygen to generate): " ENCRYPTKEY
echo -ne "\nUsing ${ENCRYPTKEY} as the encryption key\n"

read -p "Enter location of datacenter [$DEFAULTDATACENTER]: " DATACENTER
DATACENTER=${DATACENTER:-$DEFAULTDATACENTER}

read -p "Enter DNS resolver address [$DEFAULTRESOLVER]: " RESOLVER
RESOLVER=${RESOLVER:-$DEFAULTRESOLVER}

echo "Which agent config you want to install?"

# Operating system names are used here as a data source
select typeSelection in Bootstrap Server Client
do
    case $typeSelection in
        Bootstrap)
            TYPE="bootstrap"
            break
            ;;
        Server)
            TYPE="server"
            break
            ;;
        Client)
            TYPE="client"
            break
            ;;
        *)
            echo "Unknown selection."
            ;;
    esac
done

echo "You have selected ${typeSelection}"


# Run the real installation

apt-get update
apt-get install unzip -y
wget https://releases.hashicorp.com/consul/$CONSULVERSION/consul_${CONSULVERSION}_linux_amd64.zip
unzip consul_${CONSULVERSION}_linux_amd64.zip
mv consul /usr/sbin/consul
rm consul_${CONSULVERSION}_linux_amd64.zip


# Create user
groupadd --system consul
useradd -s /sbin/nologin --system -g consul consul

# Create required directory
mkdir -p /var/consul
mkdir -p /etc/consul.d/


if [[ "$TYPE" == "bootstrap" ]]; then
    cp config/bootstrap.json /etc/consul.d/agent.json
elif [[ "$TYPE" == "server" ]]; then
    cp config/server.json /etc/consul.d/agent.json
elif [[ "$TYPE" == "client" ]]; then
    cp config/client.json /etc/consul.d/agent.json
fi

sed -i "s^ENCRYPTKEY^$ENCRYPTKEY^g" /etc/consul.d/agent.json
sed -i "s/DATACENTER/$DATACENTER/g" /etc/consul.d/agent.json



chown -R consul:consul /etc/consul.d
chown -R consul:consul /var/consul
chmod -R 775 /var/consul


apt-get install dnsmasq
rm /etc/resolv.conf

# disable systemd-resolved
systemctl disable systemd-resolved
systemctl stop systemd-resolved


echo "listen-address=127.0.0.1
port=53
no-resolv
no-poll
server=${RESOLVER}
domain-needed" > /etc/dnsmasq.conf

echo "server=/consul/127.0.0.1#8600" > /etc/dnsmasq.d/consul

systemctl enable dnsmasq
systemctl start dnsmasq



echo "[Unit]
Description=Consul Service Discovery Agent
Documentation=https://www.consul.io/
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=consul
Group=consul
ExecStart=/usr/sbin/consul agent -config-dir=/etc/consul.d

ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier=consul

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/consul.service

systemctl daemon-reload
systemctl start consul
systemctl enable consul
systemctl restart consul

systemctl restart dnsmasq